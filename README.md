# Einleitung
Mit der folgenden Datenschutzerklärung möchten wir Sie darüber aufklären, welche Arten Ihrer 
personenbezogenen Daten (nachfolgend auch kurz als "Daten“ bezeichnet) wir zu welchen Zwecken und in welchem Umfang im Rahmen der Bereitstellung unserer Applikation verarbeiten.

Die verwendeten Begriffe sind nicht geschlechtsspezifisch.

Stand: 26. Januar 2022

# Verantwortlicher
Dejan Kostyszyn

Magnificent Enterprises UG (haftungsbeschränkt)

Im Gaisbühl 14a

79294 Sölden

E-Mail-Adresse: dkostyszyn@magnificent-enterprises.de

# Google AdMob
Wir verwenden Google [AdMob](https://admob.google.com/home/). Google AdMob ist ein Dienst vom amerikanischen Unternehmen Google Inc. für mobile Werbung. Im europäischen Raum ist das Unternehmen Google Ireland Limited verantwortlich für alle Google Dienste. Wir weisen darauf hin, dass Google Daten von Ihnen verarbeitet, unter anderem in den Vereinigten Staaten von Amerika. Es werden dazu möglicher Weise Daten von Ihnen ins europäische Ausland übermittelt. Ebenso weisen wir darauf hin, dass laut europäischem Gerichtshof aktuell kein angemessenes Schutzniveau für den Datentransfer in die USA existiert. Dennoch hat sich Google dazu verpflichtet auch im außereuropäischen Land die innereuropäischen Datenschutzverpflichtungen einzuhalten. Die Datenverarbeitungsbedingungen unter anderem für AdMob finden Sie unter https://business.safety.google/adscontrollerterms/. In der Privacy Policy finden Sie weitere Informationen über die Daten, die bei Verwendung von AdMob verarbeitet werden: https://policies.google.com/privacy?hl=de.


# Änderung und Aktualisierung der Datenschutzerklärung
Wir bitten Sie, sich regelmäßig über den Inhalt unserer Datenschutzerklärung zu informieren. 
Wir passen die Datenschutzerklärung an, sobald die Änderungen der von uns durchgeführten Datenverarbeitungen 
dies erforderlich machen. Wir informieren Sie, sobald durch die Änderungen eine Mitwirkungshandlung 
Ihrerseits (z.B. Einwilligung) oder eine sonstige individuelle Benachrichtigung erforderlich wird.

Sofern wir in dieser Datenschutzerklärung Adressen und Kontaktinformationen von Unternehmen und Organisationen angeben, bitten wir zu beachten, dass die Adressen sich über die Zeit ändern können und bitten die Angaben vor Kontaktaufnahme zu prüfen.
